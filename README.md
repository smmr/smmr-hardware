# Introduction

Specific hardware design for [SMMR](https://gitlab.com/smmr/smmr/).

## Instruction for building

There is a conflict between and internal bootstrap (IO12) and an external SD connected by 4 wires (SDMMC). More info see [here](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/peripherals/sd_pullup_requirements.html#strapping-conflicts-dat2).

So for building the board with ESP32-WROOM-32U, ESP32-WROOM-32U you need to program the flash as 3.3V with:

```
esptool/espefuse.py set_flash_voltage 3.3V
```

For this I suggest two options:

1. Program the ESP32 before soldering with an adapter like this one: 

2. Do not solder IO12 (Pin ), program the fuse and then solder the pin.

# Other branches

## sd

SD branch preserves the previous design (with analogic and digital inputs) but with 1 wire connection to SD.

## tiny

Tiny branch is a small design without analog and digital inputs neither the sd card. It only supports connections to GPS and I2C sensors. And only the schematic is available.
