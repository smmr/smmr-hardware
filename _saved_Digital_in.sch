EESchema Schematic File Version 4
LIBS:SMMR-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title "Sistema de Monitoreo de Material Rodante"
Date "2019-06-13"
Rev "1.3"
Comp "Sebastián Guarino"
Comment1 "Licencia MIT, ver archivo LICENSE"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R?
U 1 1 52C9FA85
P 2950 1300
AR Path="/5CE72594/52C9FA85" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/52C9FA85" Ref="R?"  Part="1" 
AR Path="/5D055DBB/52C9FA85" Ref="R401"  Part="1" 
F 0 "R401" V 3030 1300 40  0000 C CNN
F 1 "3.3k" V 2957 1301 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2880 1300 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 2950 1300 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 6 "RES SMD 3.3K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "RC0805JR-073K3L" H 0   0   50  0001 C CNN "Manf#"
F 8 "311-3.3KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    2950 1300
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 52C9FABA
P 4300 1700
AR Path="/5CE72594/52C9FABA" Ref="#PWR?"  Part="1" 
AR Path="/5D042DB3/5D046D23/52C9FABA" Ref="#PWR?"  Part="1" 
AR Path="/5D055DBB/52C9FABA" Ref="#PWR0401"  Part="1" 
F 0 "#PWR0401" H 4300 1700 30  0001 C CNN
F 1 "GND" H 4300 1630 30  0001 C CNN
F 2 "" H 4300 1700 60  0000 C CNN
F 3 "" H 4300 1700 60  0000 C CNN
	1    4300 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 1700 4300 1650
Text Label 1950 1650 2    60   ~ 0
COM
Text HLabel 5000 1450 2    60   Output ~ 0
DIN0
$Comp
L power:+3.3V #PWR?
U 1 1 52C9FB0E
P 4700 1050
AR Path="/5CE72594/52C9FB0E" Ref="#PWR?"  Part="1" 
AR Path="/5D042DB3/5D046D23/52C9FB0E" Ref="#PWR?"  Part="1" 
AR Path="/5D055DBB/52C9FB0E" Ref="#PWR0403"  Part="1" 
F 0 "#PWR0403" H 4700 1010 30  0001 C CNN
F 1 "+3.3V" H 4700 1160 30  0000 C CNN
F 2 "" H 4700 1050 60  0000 C CNN
F 3 "" H 4700 1050 60  0000 C CNN
	1    4700 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 1300 3400 1300
Text Label 2450 1300 0    60   ~ 0
IN0
Text Label 2450 2650 0    60   ~ 0
IN1
Text Label 1600 1750 2    60   ~ 0
IN0
Text Label 1950 1850 2    60   ~ 0
IN1
Text Label 1600 1950 2    60   ~ 0
IN2
Text Label 1950 2050 2    60   ~ 0
IN3
Text Label 6550 3100 0    60   ~ 0
IN2
Text Label 6550 4650 0    60   ~ 0
IN3
$Comp
L Isolator:TLP290-4 U?
U 4 1 53F1371A
P 3900 1550
AR Path="/5CE72594/53F1371A" Ref="U?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F1371A" Ref="U?"  Part="1" 
AR Path="/5D055DBB/53F1371A" Ref="U401"  Part="4" 
AR Path="/53F1371A" Ref="U?"  Part="1" 
F 0 "U401" H 3900 1850 40  0000 C CNN
F 1 "TLP290-4" H 3900 1750 40  0000 C CNN
F 2 "Package_SO:SSOP-16_4.4x5.2mm_P0.65mm" H 3736 1376 29  0001 C CNN
F 3 "$KIPRJMOD/components/MCP6024/TLP290-4_datasheet_en_20151028.pdf" H 3900 1550 60  0001 C CNN
F 4 "Toshiba Semiconductor and Storage" H 3900 1550 50  0001 C CNN "Manf"
F 5 "TLP290-4(TP,E" H 3900 1550 50  0001 C CNN "Manf#"
F 6 "Optoisolator 16 SOIC" H 3900 1550 50  0001 C CNN "Desc"
F 7 "Digikey" H 3900 1550 50  0001 C CNN "Provider"
F 8 "TLP290-4(TPECT-ND" H 3900 1550 50  0001 C CNN "Provider#"
	4    3900 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 53F1373B
P 3200 1550
AR Path="/5CE72594/53F1373B" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F1373B" Ref="R?"  Part="1" 
AR Path="/5D055DBB/53F1373B" Ref="R403"  Part="1" 
F 0 "R403" V 3280 1550 40  0000 C CNN
F 1 "4.7k" V 3207 1551 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3130 1550 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 3200 1550 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "RC0805JR-074K7L" H 0   0   50  0001 C CNN "Manf#"
F 6 "RES SMD 4.7K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "311-4.7KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    3200 1550
	-1   0    0    1   
$EndComp
Text Label 2450 1800 0    60   ~ 0
COM
Wire Wire Line
	3400 1300 3400 1450
Wire Wire Line
	3400 1800 3400 1650
$Comp
L Device:R R?
U 1 1 53F13DDB
P 2950 2650
AR Path="/5CE72594/53F13DDB" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F13DDB" Ref="R?"  Part="1" 
AR Path="/5D055DBB/53F13DDB" Ref="R402"  Part="1" 
F 0 "R402" V 3030 2650 40  0000 C CNN
F 1 "3.3k" V 2957 2651 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2880 2650 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 2950 2650 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 6 "RES SMD 3.3K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "RC0805JR-073K3L" H 0   0   50  0001 C CNN "Manf#"
F 8 "311-3.3KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    2950 2650
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 53F13DE1
P 4700 2600
AR Path="/5CE72594/53F13DE1" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F13DE1" Ref="R?"  Part="1" 
AR Path="/5D055DBB/53F13DE1" Ref="R406"  Part="1" 
F 0 "R406" V 4780 2600 40  0000 C CNN
F 1 "100k" V 4707 2601 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4630 2600 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 4700 2600 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "RC0805JR-07100KL" H 0   0   50  0001 C CNN "Manf#"
F 6 "RES SMD 100K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "311-100KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    4700 2600
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 53F13DE7
P 4300 3050
AR Path="/5CE72594/53F13DE7" Ref="#PWR?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F13DE7" Ref="#PWR?"  Part="1" 
AR Path="/5D055DBB/53F13DE7" Ref="#PWR0402"  Part="1" 
F 0 "#PWR0402" H 4300 3050 30  0001 C CNN
F 1 "GND" H 4300 2980 30  0001 C CNN
F 2 "" H 4300 3050 60  0000 C CNN
F 3 "" H 4300 3050 60  0000 C CNN
	1    4300 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3050 4300 3000
Text HLabel 5000 2800 2    60   Output ~ 0
DIN1
$Comp
L power:+3.3V #PWR?
U 1 1 53F13DF0
P 4700 2400
AR Path="/5CE72594/53F13DF0" Ref="#PWR?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F13DF0" Ref="#PWR?"  Part="1" 
AR Path="/5D055DBB/53F13DF0" Ref="#PWR0404"  Part="1" 
F 0 "#PWR0404" H 4700 2360 30  0001 C CNN
F 1 "+3.3V" H 4700 2510 30  0000 C CNN
F 2 "" H 4700 2400 60  0000 C CNN
F 3 "" H 4700 2400 60  0000 C CNN
	1    4700 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2650 3400 2650
$Comp
L Isolator:TLP290-4 U?
U 3 1 53F13DF9
P 3900 2900
AR Path="/5CE72594/53F13DF9" Ref="U?"  Part="2" 
AR Path="/5D042DB3/5D046D23/53F13DF9" Ref="U?"  Part="2" 
AR Path="/5D055DBB/53F13DF9" Ref="U401"  Part="3" 
AR Path="/53F13DF9" Ref="U?"  Part="2" 
F 0 "U401" H 3900 3200 40  0000 C CNN
F 1 "TLP290-4" H 3900 3100 40  0000 C CNN
F 2 "Package_SO:SSOP-16_4.4x5.2mm_P0.65mm" H 3736 2726 29  0001 C CNN
F 3 "$KIPRJMOD/components/MCP6024/TLP290-4_datasheet_en_20151028.pdf" H 3900 2900 60  0001 C CNN
F 4 "Toshiba Semiconductor and Storage" H 0   0   50  0001 C CNN "Manf"
F 5 "TLP290-4(TP,E" H 0   0   50  0001 C CNN "Manf#"
F 6 "Optoisolator 16 SOIC" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "TLP290-4(TPECT-ND" H 0   0   50  0001 C CNN "Provider#"
	3    3900 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 53F13DFF
P 3200 2900
AR Path="/5CE72594/53F13DFF" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F13DFF" Ref="R?"  Part="1" 
AR Path="/5D055DBB/53F13DFF" Ref="R404"  Part="1" 
F 0 "R404" V 3280 2900 40  0000 C CNN
F 1 "4.7k" V 3207 2901 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3130 2900 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 3200 2900 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "RC0805JR-074K7L" H 0   0   50  0001 C CNN "Manf#"
F 6 "RES SMD 4.7K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "311-4.7KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    3200 2900
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 2650 3400 2800
Wire Wire Line
	3400 3150 3400 3000
$Comp
L Device:R R?
U 1 1 53F13FC7
P 7000 3100
AR Path="/5CE72594/53F13FC7" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F13FC7" Ref="R?"  Part="1" 
AR Path="/5D055DBB/53F13FC7" Ref="R407"  Part="1" 
F 0 "R407" V 7080 3100 40  0000 C CNN
F 1 "3.3k" V 7007 3101 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6930 3100 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 7000 3100 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 6 "RES SMD 3.3K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "RC0805JR-073K3L" H 0   0   50  0001 C CNN "Manf#"
F 8 "311-3.3KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    7000 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 53F13FCD
P 8800 3050
AR Path="/5CE72594/53F13FCD" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F13FCD" Ref="R?"  Part="1" 
AR Path="/5D055DBB/53F13FCD" Ref="R411"  Part="1" 
F 0 "R411" V 8880 3050 40  0000 C CNN
F 1 "100k" V 8807 3051 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8730 3050 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 8800 3050 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "RC0805JR-07100KL" H 0   0   50  0001 C CNN "Manf#"
F 6 "RES SMD 100K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "311-100KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    8800 3050
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 53F13FD3
P 8400 3500
AR Path="/5CE72594/53F13FD3" Ref="#PWR?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F13FD3" Ref="#PWR?"  Part="1" 
AR Path="/5D055DBB/53F13FD3" Ref="#PWR0407"  Part="1" 
F 0 "#PWR0407" H 8400 3500 30  0001 C CNN
F 1 "GND" H 8400 3430 30  0001 C CNN
F 2 "" H 8400 3500 60  0000 C CNN
F 3 "" H 8400 3500 60  0000 C CNN
	1    8400 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 3500 8400 3450
Wire Wire Line
	7300 3100 7500 3100
$Comp
L Isolator:TLP290-4 U?
U 2 1 53F13FDE
P 8000 3350
AR Path="/5CE72594/53F13FDE" Ref="U?"  Part="3" 
AR Path="/5D042DB3/5D046D23/53F13FDE" Ref="U?"  Part="3" 
AR Path="/5D055DBB/53F13FDE" Ref="U401"  Part="2" 
AR Path="/53F13FDE" Ref="U?"  Part="3" 
F 0 "U401" H 8000 3650 40  0000 C CNN
F 1 "TLP290-4" H 8000 3550 40  0000 C CNN
F 2 "Package_SO:SSOP-16_4.4x5.2mm_P0.65mm" H 7836 3176 29  0001 C CNN
F 3 "$KIPRJMOD/components/MCP6024/TLP290-4_datasheet_en_20151028.pdf" H 8000 3350 60  0001 C CNN
F 4 "Toshiba Semiconductor and Storage" H 0   0   50  0001 C CNN "Manf"
F 5 "TLP290-4(TP,E" H 0   0   50  0001 C CNN "Manf#"
F 6 "Optoisolator 16 SOIC" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "TLP290-4(TPECT-ND" H 0   0   50  0001 C CNN "Provider#"
	2    8000 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 53F13FE4
P 7300 3350
AR Path="/5CE72594/53F13FE4" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F13FE4" Ref="R?"  Part="1" 
AR Path="/5D055DBB/53F13FE4" Ref="R408"  Part="1" 
F 0 "R408" V 7380 3350 40  0000 C CNN
F 1 "4.7k" V 7307 3351 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7230 3350 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 7300 3350 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "RC0805JR-074K7L" H 0   0   50  0001 C CNN "Manf#"
F 6 "RES SMD 4.7K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "311-4.7KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    7300 3350
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 3100 7500 3250
Wire Wire Line
	7500 3600 7500 3450
$Comp
L Device:R R?
U 1 1 53F14006
P 7000 4650
AR Path="/5CE72594/53F14006" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F14006" Ref="R?"  Part="1" 
AR Path="/5D055DBB/53F14006" Ref="R409"  Part="1" 
F 0 "R409" V 7080 4650 40  0000 C CNN
F 1 "3.3k" V 7007 4651 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6930 4650 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 7000 4650 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "RC0805JR-073K3L" H 0   0   50  0001 C CNN "Manf#"
F 6 "RES SMD 3.3K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "311-3.3KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    7000 4650
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 53F1400C
P 8800 4550
AR Path="/5CE72594/53F1400C" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F1400C" Ref="R?"  Part="1" 
AR Path="/5D055DBB/53F1400C" Ref="R412"  Part="1" 
F 0 "R412" V 8880 4550 40  0000 C CNN
F 1 "100k" V 8807 4551 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8730 4550 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 8800 4550 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "RC0805JR-07100KL" H 0   0   50  0001 C CNN "Manf#"
F 6 "RES SMD 100K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "311-100KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    8800 4550
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 53F14012
P 8400 5050
AR Path="/5CE72594/53F14012" Ref="#PWR?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F14012" Ref="#PWR?"  Part="1" 
AR Path="/5D055DBB/53F14012" Ref="#PWR0408"  Part="1" 
F 0 "#PWR0408" H 8400 5050 30  0001 C CNN
F 1 "GND" H 8400 4980 30  0001 C CNN
F 2 "" H 8400 5050 60  0000 C CNN
F 3 "" H 8400 5050 60  0000 C CNN
	1    8400 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 5050 8400 5000
$Comp
L Isolator:TLP290-4 U?
U 1 1 53F1401D
P 8000 4900
AR Path="/5CE72594/53F1401D" Ref="U?"  Part="4" 
AR Path="/5D042DB3/5D046D23/53F1401D" Ref="U?"  Part="4" 
AR Path="/5D055DBB/53F1401D" Ref="U401"  Part="1" 
AR Path="/53F1401D" Ref="U?"  Part="4" 
F 0 "U401" H 8000 5200 40  0000 C CNN
F 1 "TLP290-4" H 8000 5100 40  0000 C CNN
F 2 "Package_SO:SSOP-16_4.4x5.2mm_P0.65mm" H 7836 4726 29  0001 C CNN
F 3 "$KIPRJMOD/components/MCP6024/TLP290-4_datasheet_en_20151028.pdf" H 8000 4900 60  0001 C CNN
F 4 "Toshiba Semiconductor and Storage" H 0   0   50  0001 C CNN "Manf"
F 5 "TLP290-4(TP,E" H 0   0   50  0001 C CNN "Manf#"
F 6 "Optoisolator 16 SOIC" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "TLP290-4(TPECT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    8000 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 53F14023
P 7300 4900
AR Path="/5CE72594/53F14023" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F14023" Ref="R?"  Part="1" 
AR Path="/5D055DBB/53F14023" Ref="R410"  Part="1" 
F 0 "R410" V 7380 4900 40  0000 C CNN
F 1 "4.7k" V 7307 4901 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7230 4900 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 7300 4900 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "RC0805JR-074K7L" H 0   0   50  0001 C CNN "Manf#"
F 6 "RES SMD 4.7K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "311-4.7KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    7300 4900
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 4650 7500 4800
Wire Wire Line
	7500 5150 7500 5000
Text Label 2450 3150 0    60   ~ 0
COM
Text Label 6550 3600 0    60   ~ 0
COM
Text Label 6550 5150 0    60   ~ 0
COM
$Comp
L power:+3.3V #PWR?
U 1 1 53F1433A
P 8800 2850
AR Path="/5CE72594/53F1433A" Ref="#PWR?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F1433A" Ref="#PWR?"  Part="1" 
AR Path="/5D055DBB/53F1433A" Ref="#PWR0409"  Part="1" 
F 0 "#PWR0409" H 8800 2810 30  0001 C CNN
F 1 "+3.3V" H 8800 2960 30  0000 C CNN
F 2 "" H 8800 2850 60  0000 C CNN
F 3 "" H 8800 2850 60  0000 C CNN
	1    8800 2850
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 53F14340
P 8800 4300
AR Path="/5CE72594/53F14340" Ref="#PWR?"  Part="1" 
AR Path="/5D042DB3/5D046D23/53F14340" Ref="#PWR?"  Part="1" 
AR Path="/5D055DBB/53F14340" Ref="#PWR0410"  Part="1" 
F 0 "#PWR0410" H 8800 4260 30  0001 C CNN
F 1 "+3.3V" H 8800 4410 30  0000 C CNN
F 2 "" H 8800 4300 60  0000 C CNN
F 3 "" H 8800 4300 60  0000 C CNN
	1    8800 4300
	1    0    0    -1  
$EndComp
Text HLabel 9100 3250 2    60   Output ~ 0
DIN2
Text HLabel 9100 4800 2    60   Output ~ 0
DIN3
Wire Wire Line
	2450 1800 3200 1800
Wire Wire Line
	2450 3150 3200 3150
Wire Wire Line
	6550 3600 7300 3600
Wire Wire Line
	6550 5150 7300 5150
$Comp
L Device:R R?
U 1 1 52C9FAAD
P 4700 1250
AR Path="/5CE72594/52C9FAAD" Ref="R?"  Part="1" 
AR Path="/5D042DB3/5D046D23/52C9FAAD" Ref="R?"  Part="1" 
AR Path="/5D055DBB/52C9FAAD" Ref="R405"  Part="1" 
F 0 "R405" V 4780 1250 40  0000 C CNN
F 1 "100k" V 4707 1251 40  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4630 1250 30  0001 C CNN
F 3 "$KIPRJMOD/components/Resistors/PYu-RC_Group_51_RoHS_L_10.pdf" H 4700 1250 30  0001 C CNN
F 4 "Yageo" H 0   0   50  0001 C CNN "Manf"
F 5 "RC0805JR-07100KL" H 0   0   50  0001 C CNN "Manf#"
F 6 "RES SMD 100K OHM 5% 1/8W 0805" H 0   0   50  0001 C CNN "Desc"
F 7 "Digikey" H 0   0   50  0001 C CNN "Provider"
F 8 "311-100KARCT-ND" H 0   0   50  0001 C CNN "Provider#"
	1    4700 1250
	-1   0    0    1   
$EndComp
Wire Wire Line
	3200 1300 3200 1400
Wire Wire Line
	3200 1300 3100 1300
Connection ~ 3200 1300
Wire Wire Line
	3200 1700 3200 1800
Connection ~ 3200 1800
Wire Wire Line
	3200 1800 3400 1800
Wire Wire Line
	2450 1300 2800 1300
Wire Wire Line
	3200 3050 3200 3150
Connection ~ 3200 3150
Wire Wire Line
	3200 3150 3400 3150
Wire Wire Line
	3200 2650 3200 2750
Wire Wire Line
	3200 2650 3100 2650
Connection ~ 3200 2650
Wire Wire Line
	2450 2650 2800 2650
Wire Wire Line
	4700 1100 4700 1050
Wire Wire Line
	4700 2750 4700 2800
Connection ~ 4700 2800
Wire Wire Line
	4700 2800 5000 2800
Wire Wire Line
	4700 1400 4700 1450
Connection ~ 4700 1450
Wire Wire Line
	4700 1450 5000 1450
Wire Wire Line
	4700 2450 4700 2400
Wire Wire Line
	7300 3100 7300 3200
Connection ~ 7300 3100
Wire Wire Line
	7300 3500 7300 3600
Connection ~ 7300 3600
Wire Wire Line
	7300 3600 7500 3600
Wire Wire Line
	7300 5050 7300 5150
Connection ~ 7300 5150
Wire Wire Line
	7300 5150 7500 5150
Wire Wire Line
	9100 3250 8800 3250
Wire Wire Line
	9100 4800 8800 4800
Wire Wire Line
	8800 2850 8800 2900
Wire Wire Line
	8800 3200 8800 3250
Connection ~ 8800 3250
Wire Wire Line
	8800 4700 8800 4800
Connection ~ 8800 4800
Wire Wire Line
	8800 4300 8800 4400
Wire Wire Line
	7300 4750 7300 4650
Connection ~ 7300 4650
Wire Wire Line
	7300 4650 7500 4650
Text Notes 850  800  0    50   ~ 0
IN0, IN1, IN2, IN3: 0-10V
$Comp
L Connector:Screw_Terminal_01x02 J402
U 1 1 5D0AC867
P 1050 2050
F 0 "J402" H 1250 2050 50  0000 C CNN
F 1 "Screw_Terminal_01x02" H 1100 1650 50  0001 C CNN
F 2 "Bornier-blue:DG306-5.0-02P" H 1050 2050 50  0001 C CNN
F 3 "$KIPRJMOD/components/Bornier-blue/DG306-5.0.pdf" H 1050 2050 50  0001 C CNN
F 4 "Degson" H 0   100 50  0001 C CNN "Manf"
F 5 "DG306-5.0-02P-1B" H 0   100 50  0001 C CNN "Manf#"
F 6 "TERM BLOCK 5.08MM VERT 2POS PCB" H 0   100 50  0001 C CNN "Desc"
F 7 "Microelectronica SH" H 0   0   50  0001 C CNN "Provider"
F 8 "B2P" H 0   0   50  0001 C CNN "Provider#"
	1    1050 2050
	-1   0    0    1   
$EndComp
Wire Wire Line
	1250 1750 1600 1750
Wire Wire Line
	1250 1850 1950 1850
Wire Wire Line
	1250 1950 1600 1950
Wire Wire Line
	1250 2050 1950 2050
Wire Wire Line
	1250 1650 1950 1650
$Comp
L Connector:Screw_Terminal_01x03 J401
U 1 1 5D145CD4
P 1050 1750
F 0 "J401" H 1250 1850 50  0000 C CNN
F 1 "Screw_Terminal_01x03" H 970 1976 50  0001 C CNN
F 2 "Bornier-blue:DG306-5.0-03P" H 1050 1750 50  0001 C CNN
F 3 "$KIPRJMOD/components/Bornier-blue/DG306-5.0.pdf" H 1050 1750 50  0001 C CNN
F 4 "Degson" H 0   0   50  0001 C CNN "Manf"
F 5 "DG306-5.0-03P-1B" H 0   0   50  0001 C CNN "Manf#"
F 6 "TERM BLOCK 5.08MM VERT 3POS PCB" H 0   0   50  0001 C CNN "Desc"
F 7 "Microelectronica SH" H 0   0   50  0001 C CNN "Provider"
F 8 "B3P" H 0   0   50  0001 C CNN "Provider#"
	1    1050 1750
	-1   0    0    1   
$EndComp
Wire Wire Line
	4200 1450 4700 1450
Wire Wire Line
	4200 1650 4300 1650
Wire Wire Line
	3400 1650 3600 1650
Wire Wire Line
	3400 1450 3600 1450
Wire Wire Line
	4200 2800 4700 2800
Wire Wire Line
	4200 3000 4300 3000
Wire Wire Line
	3400 3000 3600 3000
Wire Wire Line
	3400 2800 3600 2800
Wire Wire Line
	8300 3250 8800 3250
Wire Wire Line
	8300 3450 8400 3450
Wire Wire Line
	7500 3450 7700 3450
Wire Wire Line
	7500 3250 7700 3250
Wire Wire Line
	8300 4800 8800 4800
Wire Wire Line
	8300 5000 8400 5000
Wire Wire Line
	7500 5000 7700 5000
Wire Wire Line
	7500 4800 7700 4800
Wire Wire Line
	7300 3100 7150 3100
Wire Wire Line
	6850 3100 6550 3100
Wire Wire Line
	7300 4650 7150 4650
Wire Wire Line
	6850 4650 6550 4650
$EndSCHEMATC
