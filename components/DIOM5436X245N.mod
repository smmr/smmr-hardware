PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
DIOM5436X245N
$EndINDEX
$MODULE DIOM5436X245N
Po 0 0 0 15 00000000 00000000 ~~
Li DIOM5436X245N
Cd 
Sc 00000000
At SMD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -1.0486 -9.92859 3.3828 3.3828 0 0.05 N V 21 "DIOM5436X245N"
T1 0.4248 10.2253 3.39191 3.39191 0 0.05 N V 21 "VAL**"
DS -2.7 -1.8 2.7 -1.8 0.2 21
DS -2.7 1.8 2.7 1.8 0.2 21
DC -3.95 0 -3.80858 0 0.2 21
DS -3.55 -2.25 3.55 -2.25 0.05 26
DS 3.55 -2.25 3.55 2.25 0.05 26
DS 3.55 2.25 -3.55 2.25 0.05 26
DS -3.55 2.25 -3.55 -2.25 0.05 26
DS -2.7 -1.8 -2.7 -1.4 0.2 21
DS 2.7 -1.8 2.7 -1.4 0.2 21
DS -2.7 1.8 -2.7 1.45 0.2 21
DS 2.7 1.8 2.7 1.45 0.2 21
$PAD
Sh "C" R 2.2 2.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po -2.2 0
$EndPAD
$PAD
Sh "A" R 2.2 2.15 0 0 0
At SMD N 00888000
.SolderMask 0
.SolderPaste 0
Ne 0 ""
Po 2.2 0
$EndPAD
$EndMODULE DIOM5436X245N
