 
 The extended DAT lines (DAT1-DAT3) are input on power up. They start to operate as DAT lines after the SET_BUS_WIDTH command. It is the responsibility of the host designer to connect external pull-up resistors to all data lines even if only DAT0 is to be used. If not, there may be unexpected high current consumption due to the floating inputs of DAT1 & DAT2 (if they are not used). " 
