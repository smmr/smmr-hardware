EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title "Sistema de Monitoreo de Material Rodante"
Date "2019-06-13"
Rev "1.3"
Comp "Sebastián Guarino"
Comment1 "Licencia MIT, ver archivo LICENSE"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 900  800  550  800 
U 5D03A6CB
F0 "Power_Supply" 50
F1 "Power_Supply.sch" 50
$EndSheet
$Sheet
S 4400 3050 750  1000
U 5D055DBB
F0 "Digital_in" 50
F1 "Digital_in.sch" 50
F2 "DIN0" O R 5150 3150 50 
F3 "DIN1" O R 5150 3250 50 
F4 "DIN2" O R 5150 3350 50 
F5 "DIN3" O R 5150 3450 50 
$EndSheet
Wire Wire Line
	5150 3150 6150 3150
Wire Wire Line
	6150 3250 5150 3250
Wire Wire Line
	5150 3350 6150 3350
Wire Wire Line
	6150 3450 5150 3450
$Sheet
S 4400 4800 750  750 
U 5D05F1DF
F0 "Analog_in" 50
F1 "Analog_in.sch" 50
F2 "AIN0" O R 5150 5000 50 
F3 "AIN1" O R 5150 5150 50 
$EndSheet
Wire Wire Line
	5150 5000 6150 5000
Wire Wire Line
	6150 5150 5150 5150
$Sheet
S 6150 1350 1700 4400
U 5D042DB3
F0 "ESP32" 50
F1 "ESP32.sch" 50
F2 "DIN0" I L 6150 3150 50 
F3 "DIN2" I L 6150 3350 50 
F4 "DIN3" I L 6150 3450 50 
F5 "DIN1" I L 6150 3250 50 
F6 "AIN0" I L 6150 5000 50 
F7 "AIN1" I L 6150 5150 50 
$EndSheet
$Comp
L SMMR-rescue:MountingHole-Mechanical H1
U 1 1 5D0BC4EA
P 1700 6100
F 0 "H1" H 1800 6146 50  0000 L CNN
F 1 "MountingHole" H 1800 6055 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 1700 6100 50  0001 C CNN
F 3 "~" H 1700 6100 50  0001 C CNN
	1    1700 6100
	1    0    0    -1  
$EndComp
$Comp
L SMMR-rescue:MountingHole-Mechanical H3
U 1 1 5D0BC555
P 2600 6100
F 0 "H3" H 2700 6146 50  0000 L CNN
F 1 "MountingHole" H 2700 6055 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2600 6100 50  0001 C CNN
F 3 "~" H 2600 6100 50  0001 C CNN
	1    2600 6100
	1    0    0    -1  
$EndComp
$Comp
L SMMR-rescue:MountingHole-Mechanical H2
U 1 1 5D0BC5D5
P 1700 6650
F 0 "H2" H 1800 6696 50  0000 L CNN
F 1 "MountingHole" H 1800 6605 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 1700 6650 50  0001 C CNN
F 3 "~" H 1700 6650 50  0001 C CNN
	1    1700 6650
	1    0    0    -1  
$EndComp
$Comp
L SMMR-rescue:MountingHole-Mechanical H4
U 1 1 5D0BC646
P 2600 6650
F 0 "H4" H 2700 6696 50  0000 L CNN
F 1 "MountingHole" H 2700 6605 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 2600 6650 50  0001 C CNN
F 3 "~" H 2600 6650 50  0001 C CNN
	1    2600 6650
	1    0    0    -1  
$EndComp
Wire Notes Line
	1450 5700 1450 6900
Wire Notes Line
	1450 6900 3400 6900
Wire Notes Line
	3400 6900 3400 5700
Wire Notes Line
	3400 5700 1450 5700
Text Notes 1500 5800 0    50   ~ 0
Mounting Holes
$Comp
L SMMR-rescue:MountingHole-Mechanical F1
U 1 1 5D098F81
P 3750 6100
F 0 "F1" H 3850 6146 50  0000 L CNN
F 1 "Fiducial" H 3850 6055 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Dia_2mm_Outer" H 3750 6100 50  0001 C CNN
F 3 "~" H 3750 6100 50  0001 C CNN
	1    3750 6100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
